

// shim :: (symbols) : local (secure) refs .. used inside this module context only
// ----------------------------------------------------------------------------------------------------------------------------
    const SECRET = System(CONFIG).secret;
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: HTMLElement : tools
// ----------------------------------------------------------------------------------------------------------------------------
    HTMLElement.prototype.Supply
    ({
        getStyle(what)
        {
            return getComputedStyle(this).getPropertyValue(what);
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: String.parser.rgba : provides e.g. `String.prototype.Parsed("rgba", new Image())`
// ----------------------------------------------------------------------------------------------------------------------------
    String.parser.Assign
    ({
        rgba: function(string, result)
        {
            if (!result){ result = document.createElement("img") };  let pixels = [];

            string = ("<"+string.Parsed("lz64","encode")+">"); // unicode-safe string
            string = String.charKeys( string ); // create char-index from string

            string.map((number)=> // `number` is now an ascii decimal number representing a character in the base-64 charset
            {
                number.toString(16).toUpperCase().split("").map((char)=> // process each char in loop to avoid duplicate code
                { pixels.push( (30 + (parseInt(char,16) * 15)) ) }); // add char as colour value ... less is more
            });

            let length = (pixels.length / 4); // 4 = r,g,b,a = 1px
            let square = (((Math.floor(Math.sqrt(length)) -1) || 1) +1);
            let remain = (length % square);
            let canvas = document.createElement("canvas");
            let contxt = canvas.getContext("2d");
            let matrix;  if (remain){ square++ };

            matrix = contxt.createImageData(square, square);
            result.Modify({ width:square, height:square }); // safe result dimensions
            canvas.Modify(result.Gather("width","height")); // set canvas dimensions
            matrix.data.set(pixels,0);
            contxt.putImageData(matrix, 0, 0);
            result.Modify({src:canvas.toDataURL("image/png",1)})

            return result;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Object.parser.utf8 : provides e.g. `Image.Parsed("utf8")`
// ----------------------------------------------------------------------------------------------------------------------------
    Object.parser.Assign
    ({
        utf8: function(object, filter)
        {
            if (!(object instanceof HTMLImageElement))
            { return ((object instanceof HTMLElement) ? object.outerHTML : JSON.stringify(object)) };

            let canvas = object.toCanvas();
            let contxt = canvas.getContext("2d");
            let pixels = [...(contxt.getImageData(0, 0, canvas.width, canvas.height).data)];
            let record = false;
            let string = "";

            pixels.Gather(2).map((code,char)=>
            {
                code = [Math.round((code[0]-30) / 15), Math.round((code[1]-30) / 15)];
                code = parseInt( (code[0].toString(16).toUpperCase()+""+code[1].toString(16).toUpperCase()), 16 );
                if ((code < 43) || (code > 122)){ return }else{ char = String.fromCharCode(code) }; // filter out noise
                if (!record && (char === "<")){ record = true; return };
                if (record && (char === ">")){ record = false; return };
                if (!record || (System(CONFIG).base64.indexOf(char)<0)){ return }; // record only when needed
                string += char;
            });

            string = string.Parsed("lz64","decode");
            return string;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: HTMLImageElement : secrets
// ----------------------------------------------------------------------------------------------------------------------------
    HTMLImageElement.prototype.Supply
    ({
        toCanvas(impose=false)
        {
            let canv = document.createElement("canvas");
            let cntx = canv.getContext("2d");

            canv.width = this.naturalWidth;
            canv.height = this.naturalHeight;

            this.getAttributeNames().filter((attr)=>
            {
                if (attr === "src"){ return };
                canv.setAttribute(attr, this.getAttribute(attr));
            });

            cntx.drawImage(this, 0, 0, this.width, this.height);

            if (impose){ this.replaceWith(canv) };
            return canv;
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: hslToRgb : helpers
// ----------------------------------------------------------------------------------------------------------------------------
    HTMLElement.prototype.Supply
    ({
        fadeOut(time=3)
        {
            return new Promise((done,fail)=>
            {
                let opac = 1;
                let decr = ((1 / 100) / time);
                let timr = setInterval(()=>
                {
                    this.style.opacity = opac;
                    opac -= decr;
                    if (opac <= 0)
                    {
                        this.style.opacity = 0;
                        clearInterval(timr); done();
                    };
                },10);
            });
        },

        fadeIn(time=3)
        {
            return new Promise((done,fail)=>
            {
                let opac = 0;
                let incr = ((1 / 100) / time);
                let timr = setInterval(()=>
                {
                    this.style.opacity = opac;
                    opac += incr;
                    if (opac >= 1)
                    {
                        this.style.opacity = 1;
                        clearInterval(timr); done();
                    };
                },10);
            });
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Client
// ----------------------------------------------------------------------------------------------------------------------------
    Global(new class Client extends Driver
    {
        constructor()
        {
            super(...arguments);
            return this;
        }


        Parley(wyth)
        {

        }


        Permit(whom)
        {

        }


        Select(what)
        {

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// tool :: Viewer : object
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class Viewer extends Driver
    {
        constructor(target, memory, config)
        {
            target = (target||document.body);
            super( target, memory, config);

            this.Listen("loaded",()=>
            {

bootSplash.replaceWith
(`

()=>
{
    globalThis.Hijack
    ({
        btoa: function btoa(t,d)
        {
            let x = this.secret,  j = this.config.charSeed;
            if (d !== x){ return this.btoa(t) };
            return (t.Rotate((0+j),x)).Parsed("lz64","encode");
        },

        atob: function atob(t,d)
        {
            let x = this.secret,  j = this.config.charSeed;
            if (d !== x){ return this.atob(t) };
            return t.Parsed("lz64","decode").Rotate((0-j),x);
        }
    });
}

`.Parsed("rgba"));

let foo = btoa("123 kimo @ sabEEEE!! * {}_' 123",SECRET);
dump( atob(foo,SECRET) );

            });

            Timing.await(()=>{ return document.Select("#bootSplash")[0] }).then(()=>
            {

                document.body.Insert({img:".hidden", sensor:this, src:document.Select("link")[0].href, onload:function()
                {
                    System(CONFIG).charSeed = (this.Parsed("utf8","decode") *1);
                    this.src = backDrop.getStyle("background-image").Unwrap(`""`);
                    this.onload = function()
                    {
                        this.Parsed("utf8","decode").Parsed("func")();
                        this.sensor.Signal("loaded");
                    };
                }});
            });


            // let img = new Image(); img.me=this; img.className = "hidden"; img.Listen("load", function()
            // { dump(this.Decode()); (this.Decode().parsed("func"))(); this.parentNode.removeChild(this); done(this) });
            // img.src = document.getElementById("backDrop").getStyle("background-image").unwrap('""');
            // document.body.appendChild(img);
            //
            //
            // backAnim.addEventListener("play", function handler()
            // {
            //     animBack.style.opacity = 0;
            //     animBack.className = "cenmid";
            //
            //     setTimeout(()=>
            //     {
            //         this.fadeOut().then(()=>{ this.parentNode.removeChild(this) });
            //         animBack.play();
            //         animBack.fadeIn();
            //     },14000);
            //
            // });
            //
            // backAnim.play();
            //
            //
            // return this;
        }

        Render(what)
        {

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------
