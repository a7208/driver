

// tool :: HTTPServer : Server driver
// ----------------------------------------------------------------------------------------------------------------------------
    Global(class HTTPServer extends Driver
    {
        constructor()
        {
            super(...arguments);
            // return this;
        }


        async Access(target, config)
        {
            config = (config || {}).Supply({method:"GET"});
            return new Promise(function then(done,fail)
            {
                return fetch(target,config).then((result)=>
                {
                    result.blob().then((blob)=>{ done(URL.createObjectURL(blob)) }, fail)
                });
            });
        }


        async Select(target)
        {
            return this.Access(target);
        }


        async Permit(whom)
        {

        }
    });
// ----------------------------------------------------------------------------------------------------------------------------



// tool :: Server : instance
// ----------------------------------------------------------------------------------------------------------------------------
    Global({Server: new HTTPServer()});
// ----------------------------------------------------------------------------------------------------------------------------
